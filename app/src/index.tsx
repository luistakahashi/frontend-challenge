import React from 'react';
import ReactDOM from 'react-dom';

import AppRouter from 'base/view/router';
import ErrorBoundary from 'base/view/error-boundary';

import * as serviceWorker from './serviceWorker';

import './index.scss';

ReactDOM.render(
  <ErrorBoundary>
    <AppRouter />
  </ErrorBoundary>,
  document.getElementById('root')
);
serviceWorker.unregister();
