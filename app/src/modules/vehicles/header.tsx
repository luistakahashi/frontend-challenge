import React, { FunctionComponent, useState, useEffect } from 'react';

import VehicleService, { Vehicle } from './service';

import Input, { InputData, initialInputData } from 'components/input';
import { empty, exactLength } from 'components/input/validations';

import './header.scss';

export interface VehiclesPageHeaderProps {
  onAddVehicle: (vehicle: Vehicle) => void;
}

enum VehiclesPageHeaderInput {
  vehicle = 'vehicle',
}

const VEHICLES_PAGE_HEADER_INPUTS = {
  [VehiclesPageHeaderInput.vehicle]: {
    inputProps: {
      name: VehiclesPageHeaderInput.vehicle,
      placeholder: 'Placa',
      required: true,
      type: 'text',
    },
    validations: [empty, exactLength(7), VehicleService.vehicleValidation],
  },
};

const VehiclesPageHeader: FunctionComponent<VehiclesPageHeaderProps> = ({ onAddVehicle }) => {
  const [plate, setPlate] = useState<InputData>(initialInputData);
  const [isError, setIsError] = useState<boolean>(false);
  const [submitted, setSubmitted] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>('');

  const onRequestInit = () => {
    setIsError(false);
    setErrorMessage('');
  };

  useEffect(() => {
    if (!plate.isValid || !plate.touched) return;

    const onFetchVehicles = async () => {
      onRequestInit();

      try {
        const { data: rawVehicle } = await VehicleService.addVehicle(plate);

        onAddVehicle(rawVehicle);
      } catch (error) {
        setIsError(true);
        setErrorMessage('Oops, verifique os dados da placa :(');
      }
    };

    onFetchVehicles();

    // eslint-disable-next-line
  }, [plate, submitted]);

  const handleFormSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    setSubmitted(true);
  };

  return (
    <section className="vehicles-page-header">
      <h1 className="vehicles-page-header__title"> Veículos </h1>
      <div className="vehicles-page-header__container">
        <h2 className="vehicles-page-header__subtitle"> Adicione novos veículos </h2>
        <p className="vehicles-page-header__description"> Basta digitar a placa abaixo </p>
        <form onSubmit={handleFormSubmit}>
          <Input
            {...VEHICLES_PAGE_HEADER_INPUTS[VehiclesPageHeaderInput.vehicle]}
            handleInputChange={(inputData: InputData) => setPlate(inputData)}
          />
        </form>
        {isError && <p className="vehicle-page-header__error">{errorMessage}</p>}
      </div>
    </section>
  );
};

export default VehiclesPageHeader;
