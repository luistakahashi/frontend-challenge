import FetchService from 'base/services/fetch';
import { InputData } from 'components/input';
import { InputError } from 'components/input/validations';
import { AxiosResponse } from 'axios';

enum ENDPOINTS {
  VEHICLE = 'vehicle',
}

export interface Vehicle {
  id: string;
  plate: string;
}

interface ResponseErrors {
  message: string;
}
export interface VehiclesMap {
  [plate: string]: Vehicle;
}

interface VehicleGetResponse {
  data: Vehicle[];
  error?: ResponseErrors;
}

interface VehicleDeleteResponse extends AxiosResponse {
  error?: ResponseErrors;
}

interface VehiclePostResponse {
  data: Vehicle;
  error?: ResponseErrors;
}

class VehicleService {
  public static fetchVehicles = async (): Promise<VehicleGetResponse> => {
    return await FetchService.get(ENDPOINTS.VEHICLE);
  };

  public static addVehicle = async ({ value: plate }: InputData): Promise<VehiclePostResponse> => {
    return await FetchService.post(ENDPOINTS.VEHICLE, { data: { plate } });
  };

  public static deleteVehicle = async (vehicle: Vehicle): Promise<VehicleDeleteResponse> => {
    return await FetchService.delete(
      `${ENDPOINTS.VEHICLE}/${vehicle.id}`,
      {},
      { returnResponse: true }
    );
  };

  public static vehicleValidation = (value: string): InputError => {
    const regex = new RegExp('[a-zA-Z]{3}[0-9]{4}');
    const hasError = !regex.test(value);

    return {
      hasError,
      ...(hasError && { errorMessage: `Oops, este campo precisa estar no formato XXX0000` }),
    };
  };
}

export default VehicleService;
