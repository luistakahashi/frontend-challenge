import React, { FunctionComponent, useState, useEffect, useCallback } from 'react';
import { History } from 'history';

import ScrollArea from 'react-scrollbar';

import Loading from 'components/loading';

import VehicleItem from './item';
import VehiclesPageHeader from './header';
import VehicleService, { Vehicle, VehiclesMap } from './service';

import './vehicles.scss';
import AuthService from 'base/services/auth';

export interface VehiclesPageProps {
  history: History;
}

const VehiclesPage: FunctionComponent<VehiclesPageProps> = ({ history }) => {
  const [vehicles, setVehicles] = useState<VehiclesMap>({});
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>('');

  const onRequestInit = () => {
    setIsLoading(true);
    setIsError(false);
    setErrorMessage('');
  };

  useEffect(() => {
    const onFetchVehicles = async () => {
      onRequestInit();

      try {
        const { data: rawVehicles } = await VehicleService.fetchVehicles();

        const formattedVehicles = rawVehicles.reduce(
          (vehiclesMap: VehiclesMap, vehicle: Vehicle) => {
            vehiclesMap[vehicle.plate] = vehicle;

            return vehiclesMap;
          },
          {}
        );

        setVehicles(formattedVehicles);
      } catch (error) {
        setIsError(true);
        setErrorMessage(error.message);

        if (error.response && error.response.status === 401) {
          alert('Oops parece que você não está logado, ou sua sessão expirou');
          AuthService.clearToken();

          history.push('/');
        }
      } finally {
        setIsLoading(false);
      }
    };

    onFetchVehicles();
    // eslint-disable-next-line
  }, []);

  const handleVehicleAdition = (vehicle: Vehicle) =>
    setVehicles({ ...vehicles, [vehicle.plate]: vehicle });

  const onDeleteVehicle = useCallback(
    async (vehicle: Vehicle) => {
      try {
        if (!vehicles[vehicle.plate]) return;

        onRequestInit();

        const { status } = await VehicleService.deleteVehicle(vehicle);

        if (status !== 204) {
          throw new Error(
            `Oops, houve algum erro ao tentar deletar o veículo: ${vehicle.plate} :( Tente novamente em alguns minutos`
          );
        }

        delete vehicles[vehicle.plate];

        setVehicles({ ...vehicles });
      } catch (error) {
        setIsError(true);
        setErrorMessage(error.message);
      } finally {
        setIsLoading(false);
      }
    },
    [vehicles]
  );

  if (isLoading) {
    return <Loading />;
  }

  const vehiclesList = Object.values(vehicles);
  const hasVehicles = vehiclesList.length > 0;

  return (
    <div className="vehicles-page" data-testid="vehicles-page">
      <div className="vehicles-page__container">
        <VehiclesPageHeader onAddVehicle={handleVehicleAdition} />
        {hasVehicles ? (
          <ScrollArea className="vehicles-page__list" horizontal={false}>
            {vehiclesList.map((vehicle: Vehicle, key: number) => (
              <VehicleItem key={key} vehicle={vehicle} onDeleteVehicle={onDeleteVehicle} />
            ))}
          </ScrollArea>
        ) : (
          <div> :( não há nenhum veículo cadastrado</div>
        )}

        {isError && <p className="login-page__error">{errorMessage}</p>}
      </div>
    </div>
  );
};

export default VehiclesPage;
