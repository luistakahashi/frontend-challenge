import React, { FunctionComponent } from 'react';

import { Vehicle } from '../service';

import './item.scss';

interface VehicleItemProps {
  vehicle: Vehicle;
  onDeleteVehicle: (vehicle: Vehicle) => void;
}

const VehicleItem: FunctionComponent<VehicleItemProps> = ({ vehicle, onDeleteVehicle }) => (
  <div className="vehicle-item">
    <div className="vehicle-item__plate">{vehicle.plate}</div>
    <div className="vehicle-item__delete" onClick={() => onDeleteVehicle(vehicle)}>
      Deletar
    </div>
  </div>
);

export default VehicleItem;
