import React, { FunctionComponent, useState, useEffect } from 'react';

import Button from 'components/button';
import Loading from 'components/loading';
import Input, { InputData, initialInputData } from 'components/input';
import { empty, maxLength, minLength } from 'components/input/validations';

import logo from 'assets/logo.svg';

import AuthService from 'base/services/auth';

import './login.scss';

enum LoginInputs {
  email = 'email',
  password = 'password',
}

const LOGIN_INPUTS = {
  [LoginInputs.email]: {
    inputProps: {
      name: LoginInputs.email,
      placeholder: LoginInputs.email,
      required: true,
      type: LoginInputs.email,
    },
    validations: [empty, minLength(4), maxLength(50)],
  },
  [LoginInputs.password]: {
    inputProps: {
      name: LoginInputs.password,
      placeholder: 'senha',
      required: true,
      type: LoginInputs.password,
    },
    validations: [empty, minLength(3), maxLength(20)],
  },
};

export interface LoginPageProps {
  onSetToken: (token: string | undefined) => void;
}

const LoginPage: FunctionComponent<LoginPageProps> = ({ onSetToken }) => {
  const [email, setEmail] = useState<InputData>(initialInputData);
  const [password, setPassword] = useState<InputData>(initialInputData);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isError, setIsError] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>('');
  const [submitted, setSubmitted] = useState<boolean>(false);

  const onLoginInit = () => {
    setIsLoading(true);
    setSubmitted(false);

    setIsError(false);
    setErrorMessage('');
  };

  useEffect(() => {
    const onLogin = async () => {
      onLoginInit();

      try {
        const hasFilledForm = email.touched && password.touched;
        if (!hasFilledForm) {
          return;
        }

        if (!email.isValid || !password.isValid) {
          throw new Error('Email e(ou) senha inválidos');
        }

        const {
          data: { token },
        } = await AuthService.onLogin({ email, password });

        onSetToken(token);
      } catch (error) {
        setIsError(true);
        setErrorMessage(error.message);
      } finally {
        setIsLoading(false);
      }
    };

    onLogin();
    // eslint-disable-next-line
  }, [email, password, submitted]);

  const handleFormSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    setSubmitted(true);
  };

  if (isLoading) {
    return <Loading />;
  }

  return (
    <section className="login-page" data-testid="login-page">
      <div className="login-page__container">
        <img className="login-page__logo" src={logo} alt="Easy carros Logo" />
        <form onSubmit={handleFormSubmit} autoComplete="off">
          <Input
            {...LOGIN_INPUTS[LoginInputs.email]}
            handleInputChange={(inputData: InputData) => setEmail(inputData)}
          />
          <Input
            {...LOGIN_INPUTS[LoginInputs.password]}
            handleInputChange={(inputData: InputData) => setPassword(inputData)}
          />
          <Button onClick={handleFormSubmit}>Entrar</Button>
        </form>
        {isError && <p className="login-page__error">{errorMessage}</p>}
      </div>
    </section>
  );
};

export default LoginPage;
