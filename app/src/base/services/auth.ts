import { InputData } from 'components/input';

import FetchService from './fetch';

enum StorageKeys {
  token = 'token',
}

interface LoginData {
  email: InputData;
  password: InputData;
}

interface LoginResponse {
  data: {
    token?: string;
  };
  error?: {
    message: string;
  };
}

const LOGIN_ENDPOINT = 'auth';

class AuthService {
  public static formatToken = (rawToken: string) => `Bearer ${rawToken}`;

  public static saveLocalToken(rawToken: string): string {
    const formattedToken = AuthService.formatToken(rawToken);

    localStorage.setItem(StorageKeys.token, formattedToken);

    return formattedToken;
  }

  public static getLocalToken(): string {
    return localStorage.getItem(StorageKeys.token) || '';
  }

  public static clearToken(): void {
    return localStorage.removeItem(StorageKeys.token);
  }

  public static onLogin = async ({ email, password }: LoginData): Promise<LoginResponse> => {
    return await FetchService.post(LOGIN_ENDPOINT, {
      data: { email: email.value, password: password.value },
    });
  };
}

export default AuthService;
