import axios, { AxiosRequestConfig, Method, AxiosResponse } from 'axios';
import AuthService from './auth';

const BASE_URL = 'http://localhost:8181';

interface Methods {
  GET: Method;
  PUT: Method;
  POST: Method;
  PATCH: Method;
  DELETE: Method;
}

interface CustomRequestConfig {
  returnResponse?: boolean;
}

class FetchService {
  public static METHODS: Methods = {
    DELETE: 'DELETE',
    GET: 'GET',
    PATCH: 'PATCH',
    POST: 'POST',
    PUT: 'PUT',
  };

  public static fetch(
    endpoint: string,
    opts: AxiosRequestConfig | undefined = {},
    custom: CustomRequestConfig | undefined = {}
  ): AxiosResponse | any {
    return new Promise((resolve, reject) => {
      axios(`${BASE_URL}/${endpoint}`, opts)
        .then((res: any) => {
          if (custom.returnResponse) {
            return resolve(res);
          }

          return resolve(res.data);
        })
        .catch(error => {
          if (!error.status) {
            return reject(
              new Error('Oops, houve um problema inexperado, tente novamente em alguns minutos')
            );
          }

          return reject(error);
        });
    });
  }

  public static authFetch = (
    endpoint: string,
    opts: AxiosRequestConfig,
    custom?: CustomRequestConfig
  ) => {
    const token = AuthService.getLocalToken();
    const hasToken = !!token && token.length > 0;

    const options = Object.assign(opts, {
      headers: Object.assign(opts.headers || {}, {
        'Content-Type': 'application/json',
        ...(hasToken && { Authorization: token }),
      }),
    });

    return FetchService.fetch(endpoint, options, custom);
  };

  public static fetchWithMethod = (method: Method) => (
    endpoint: string,
    opts?: AxiosRequestConfig,
    custom?: CustomRequestConfig
  ) => FetchService.authFetch(endpoint, { method, ...opts }, custom);

  public static get = FetchService.fetchWithMethod(FetchService.METHODS.GET);
  public static put = FetchService.fetchWithMethod(FetchService.METHODS.PUT);
  public static post = FetchService.fetchWithMethod(FetchService.METHODS.POST);
  public static patch = FetchService.fetchWithMethod(FetchService.METHODS.PATCH);
  public static delete = FetchService.fetchWithMethod(FetchService.METHODS.DELETE);
}

export default FetchService;
