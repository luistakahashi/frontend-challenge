import React, { FunctionComponent, useState, useEffect } from 'react';
import { History } from 'history';
import { Route, Switch } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';

import LoginPage from 'modules/login';
import VehiclesPage from 'modules/vehicles';

import AuthService from 'base/services/auth';

const APP_ROUTES = {
  HOME: '/',
  LOGIN: '/login',
  VEHICLES: '/vehicles',
};

const APP_ROUTES_COMPONENTS = {
  [APP_ROUTES.VEHICLES]: VehiclesPage,
};

interface AppProps {
  history: History;
}

const App: FunctionComponent<AppProps> = ({ history }) => {
  const [token, setToken] = useState<string>(AuthService.getLocalToken());

  const onSetToken = (rawToken: string | undefined) => {
    if (!rawToken) {
      throw new Error('Não foi fornecido nenhum token');
    }

    const formattedToken = AuthService.saveLocalToken(rawToken);

    setToken(formattedToken);
  };

  useEffect(() => {
    if (token && token.length > 0) {
      history.push(APP_ROUTES.VEHICLES);
    }
  }, [token, history]);

  const renderLoginPage = () => <LoginPage onSetToken={onSetToken} />;

  return (
    <ScrollArea className="app" horizontal={false}>
      <Switch>
        <Route path={APP_ROUTES.LOGIN} render={renderLoginPage} />
        <Route path={APP_ROUTES.VEHICLES} component={APP_ROUTES_COMPONENTS[APP_ROUTES.VEHICLES]} />
        <Route render={renderLoginPage} />
      </Switch>
    </ScrollArea>
  );
};

export default App;
