import React, { Component, ReactElement } from 'react';

import Button from 'components/button';

import './error-boundary.scss';

const onReloadPage = (): void => {
  const hasBrowserLocation = 'location' in window;
  if (!hasBrowserLocation) return;

  window.location.reload();
};

interface ErrorBoundaryProps {
  children: ReactElement;
}

interface ErrorBoundaryState {
  hasError: boolean;
}

class ErrorBoundary extends Component<ErrorBoundaryProps> {
  public state: ErrorBoundaryState = {
    hasError: false,
  };

  public componentDidCatch() {
    this.setState({ hasError: true });
  }

  public render() {
    const { hasError } = this.state;

    if (hasError) {
      return (
        <section className="error-boundary">
          <div className="error-boundary__container">
            <h1 className="error-boundary__title"> Oops, houve um problema inesperado :( </h1>
            <p className="error-boundary__text">Clique no botão abaixo para recarregar a página</p>
            <Button onClick={onReloadPage}>Recarregar</Button>
          </div>
        </section>
      );
    }

    const { children } = this.props;

    return children;
  }
}

export default ErrorBoundary;
