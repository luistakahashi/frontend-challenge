import React from 'react';
import AppRouter from './router';

import { render, getByTestId } from '@testing-library/react/';

describe('App', () => {
  it('renders without crashing', () => {
    expect(() => {
      const { container } = render(<AppRouter />);
    }).not.toThrow();
  });
  it('should render login page', () => {
    const { container } = render(<AppRouter />);

    const loginPage = getByTestId(container, 'login-page');
    expect(loginPage).toBeDefined();
    expect(container).toMatchSnapshot();
  });
});
