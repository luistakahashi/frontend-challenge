import React, { FunctionComponent } from 'react';
import { createBrowserHistory } from 'history';
import { Router, Route } from 'react-router-dom';

import App from './app';

const history = createBrowserHistory();

const AppRouter: FunctionComponent = () => (
  <section data-testid="router-container">
    <Router history={history}>
      <Route path="/" component={App} />
    </Router>
  </section>
);

export default AppRouter;
