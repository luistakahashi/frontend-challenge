import React from 'react';
import Button from './index';

import { render, getByTestId } from '@testing-library/react/';

describe('Button', () => {
  it('should match snapshot', () => {
    const buttonContent = 'Test Content bla';
    const { container } = render(<Button>{buttonContent}</Button>);
    const button = getByTestId(container, 'button');

    expect(button.textContent).toEqual(buttonContent);
    expect(button).toMatchSnapshot();
  });
});
