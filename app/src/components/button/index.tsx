import React, {
  ReactNode,
  FunctionComponent,
  FunctionComponentElement,
  ButtonHTMLAttributes,
} from 'react';

import './button.scss';

interface ButtonProps {
  children: ReactNode | string;
}

const Button: FunctionComponent<ButtonProps & ButtonHTMLAttributes<HTMLButtonElement>> = (
  props
): FunctionComponentElement<HTMLButtonElement> => (
  <button className="button" data-testid="button" {...props}>
    {props.children}
  </button>
);

export default Button;
