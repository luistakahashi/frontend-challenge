import React, {
  useState,
  useEffect,
  FunctionComponent,
  FunctionComponentElement,
  InputHTMLAttributes,
} from 'react';

import useDebounce from 'base/hooks/use-debounce';

import { InputError, InputValidation, InputValidator } from './validations';

import './input.scss';

export interface InputData {
  value: string;
  touched: boolean;
  isValid: boolean;
}

export const initialInputData: InputData = {
  isValid: false,
  touched: false,
  value: '',
};

interface InputProps {
  initialValue?: any;
  validations?: InputValidation[];
  inputProps: InputHTMLAttributes<HTMLInputElement>;
  handleInputChange: (inputData: InputData) => void;
}

const Input: FunctionComponent<InputProps> = ({
  validations,
  initialValue,
  handleInputChange,
  inputProps,
}): FunctionComponentElement<HTMLInputElement> => {
  const [touch, setTouch] = useState<boolean>(false);
  const [value, setValue] = useState<string>(initialValue);
  const [errors, setErrors] = useState<InputError[]>([]);

  const debouncedValue = useDebounce(value, 300);

  useEffect(() => {
    if (touch) {
      const inputErrors = InputValidator.validate(validations, debouncedValue);

      setErrors(inputErrors);

      handleInputChange({ value, isValid: !inputErrors.length, touched: touch });
    }
    // eslint-disable-next-line
  }, [touch, debouncedValue]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    if (!touch) setTouch(true);

    setValue(event.target.value);
  };

  return (
    <div className="input">
      <input
        autoComplete="off"
        value={value}
        className="input__element"
        onChange={handleChange}
        {...inputProps}
      />
      {errors.length > 0 && (
        <div className="input__error-container">
          {errors.map(({ errorMessage }, key) => (
            <p key={key} className="input__error">
              {errorMessage}
            </p>
          ))}
        </div>
      )}
    </div>
  );
};

Input.defaultProps = {
  initialValue: '',
  inputProps: undefined,
  validations: [],
};

export default Input;
