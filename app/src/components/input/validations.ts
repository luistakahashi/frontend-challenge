export interface InputError {
  hasError: boolean;
  errorMessage?: string;
}

const mountMessage = (message: string) => `Oops, este campo ${message} :(`;

export const empty = (value: any): InputError => {
  const hasError = !value;

  return {
    hasError,
    ...(hasError && { errorMessage: mountMessage('não pode estar vazio') }),
  };
};

export const exactLength = (length: number) => (value: any): InputError => {
  const hasError = value.length !== length;

  return {
    hasError,
    ...(hasError && { errorMessage: mountMessage(`não possui ${length} caracteres`) }),
  };
};

export const minLength = (length: number) => (value: any): InputError => {
  const hasError = value.length < length;

  return {
    hasError,
    ...(hasError && { errorMessage: mountMessage(`possui menos que ${length} caracteres`) }),
  };
};

export const maxLength = (length: number) => (value: any): InputError => {
  const hasError = value.length >= length;

  return {
    hasError,
    ...(hasError && { errorMessage: mountMessage(`possui mais que ${length} caracteres`) }),
  };
};

export type InputValidation = (value: any) => InputError;

export class InputValidator {
  public static validate = (
    validations: InputValidation[] | undefined,
    value: any
  ): InputError[] => {
    if (!validations || !validations.length) return [];

    const errors = validations.map(validation => validation(value));

    return errors.filter(({ hasError }) => !!hasError);
  };
}
