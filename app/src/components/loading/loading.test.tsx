import React from 'react';
import Loading from './index';

import { render, getByTestId } from '@testing-library/react/';

describe('Loading', () => {
  it('should match snapshot', () => {
    const { container } = render(<Loading />);
    const loading = getByTestId(container, 'loading');

    expect(loading).toMatchSnapshot();
  });
});
