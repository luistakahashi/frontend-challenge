import React, { FunctionComponent } from 'react';

import gear from 'assets/gear.svg';

import './loading.scss';

const Loading: FunctionComponent = () => (
  <section className="loading" data-testid="loading">
    <img className="loading__image" src={gear} alt="Loading gear" />
  </section>
);

export default Loading;
